package com.ArrayGenerator;


import java.util.Arrays;
import java.util.Random;

public class ArraysGenerator {

    public static int[] bestCaseArray(int arrayLength) {
        int[] array = new int[arrayLength];
        Random numbers = new Random();
        for (int i = 0; i < array.length - 1; i++) {
            array[i] = numbers.nextInt(1000);
        }
        Arrays.sort(array);
        return array;
    }

    public static int[] averageCaseArray(int arrayLength) {
        int[] array = new int[arrayLength];
        Random numbers = new Random();
        for (int i = 0; i < array.length - 1; i++) {
            array[i] = numbers.nextInt(1000);
        }
        return array;
    }

    public static int[] worstCaseArray(int arrayLength) {

        int[] array = new int[arrayLength];
        Random numbers = new Random();
        for (int i = 0; i < array.length - 1; i++) {
            array[i] = numbers.nextInt(1000);
        }
        Arrays.sort(array);
        int last = array.length - 1;
        int middle = array.length / 2;
        for (int i = 0; i <= middle; i++) {
            int temp = array[i];
            array[i] = array[last - i];
            array[last - i] = temp;
        }

        return array;
    }
}
