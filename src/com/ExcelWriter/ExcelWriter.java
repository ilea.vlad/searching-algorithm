package com.ExcelWriter;

import org.apache.poi.hssf.usermodel    .HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.io.FileOutputStream;
import java.io.IOException;

public class ExcelWriter {


    public static void ExcelWriter(String fileName, long bubbleSort[][], long quickSort[], long[] insertionSort) {
        String excelFolder = "F:\\New folder\\";
        String filePath = excelFolder + fileName + ".xls";
        int l = 0;
        try {
            FileOutputStream fileOut = new FileOutputStream(filePath);
            HSSFWorkbook wb = new HSSFWorkbook();
            Sheet sheet = wb.createSheet("FirstSheet");
            for (int i = 1; i < bubbleSort.length + 1; i++) {
                Row r = sheet.createRow(0);
                for (int c = 0; c <= 3; c++) {
                    sheet.setColumnWidth(c, 3000);
                }
                Cell c = r.createCell(0);
                c.setCellValue("Array Size");
                c = r.createCell(1);
                c.setCellValue("Bubble Sort");
                c = r.createCell(2);
                c.setCellValue("Quick Sort");
                c = r.createCell(3);
                c.setCellValue("Insertion Sort");
                r = sheet.createRow(i);
                c = r.createCell(0);
                c.setCellValue(bubbleSort[l][0]);
                c = r.createCell(1);
                c.setCellValue(bubbleSort[l][1]);
                c = r.createCell(2);
                c.setCellValue(quickSort[l]);
                c = r.createCell(3);
                c.setCellValue(insertionSort[l]);
                l++;
            }

            wb.write(fileOut);

        } catch (IOException ex) {
            System.out.println("The file could not be written");
        }
    }
}
