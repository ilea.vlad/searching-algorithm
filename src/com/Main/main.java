package com.Main;

import com.ArrayGenerator.ArraysGenerator;
import com.BubbleSort.BubbleSortAlg;
import com.ExcelWriter.ExcelWriter;
import com.InsertionSort.InsertionSortAlg;
import com.QuickSort.QuickSortAlg;

public class main {
    public static void main(String[] args) {
        ExcelWriter.ExcelWriter("BestCaseScenario",BubbleSortAlg.bubbleSort(1),QuickSortAlg.quick(1),InsertionSortAlg.insertionSort(1));
        ExcelWriter.ExcelWriter("AverageCaseScenario",BubbleSortAlg.bubbleSort(2),QuickSortAlg.quick(2),InsertionSortAlg.insertionSort(2));
        ExcelWriter.ExcelWriter("WorstCaseScenario",BubbleSortAlg.bubbleSort(3),QuickSortAlg.quick(3),InsertionSortAlg.insertionSort(3));
    }
}
