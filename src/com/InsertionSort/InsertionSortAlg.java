package com.InsertionSort;

import com.ArrayGenerator.ArraysGenerator;


public class InsertionSortAlg {
    private static int[] array;

    public static long[] insertionSort(int complexity) {
        int countComp;
        int countAssign;
        int l = 0;
        long[] insertionSort = new long[100];
        for (int k = 100; k <= 10000; k += 100) {
            int sum;
            switch (complexity) {
                case 1:
                    array = ArraysGenerator.bestCaseArray(k);
                    break;
                case 2:
                    array = ArraysGenerator.averageCaseArray(k);
                    break;
                case 3:
                    array = ArraysGenerator.worstCaseArray(k);
                    break;
            }
            countAssign = 0;
            countComp = 0;

            for (int i = 1; i < array.length; i++) {
                for (int j = i - 1; j >= 0; j--) {
                    countComp++;
                    if (array[i] < array[j]) {

                        int temp = array[j];
                        array[j] = array[i];
                        array[i] = temp;
                        countAssign += 3;
                        i--;
                    } else {
                        break;
                    }
                }


            }
            sum = countAssign + countComp;
            insertionSort[l] = sum;
            l++;
        }
        return insertionSort;
    }

}
