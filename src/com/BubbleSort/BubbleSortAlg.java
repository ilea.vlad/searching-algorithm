package com.BubbleSort;

import com.ArrayGenerator.ArraysGenerator;


public class BubbleSortAlg {
    private static int[] array;

    public static long[][] bubbleSort(int complexity) {
        long[][] bubbleSort = new long[100][2];
        int l = 0;


        for (int k = 100; k <= 10000; k += 100) {
            switch (complexity) {
                case 1:
                    array = ArraysGenerator.bestCaseArray(k);
                    break;
                case 2:
                    array = ArraysGenerator.averageCaseArray(k);
                    break;
                case 3:
                    array = ArraysGenerator.worstCaseArray(k);
                    break;
            }


            int countAssign = 0;
            int countCompara = 0;
            int countSum;
            for (int i = array.length - 1; i > 0; i--) {
                for (int j = 0; j < i; j++) {
                    countCompara++;
                    if (array[j] > array[j + 1]) {
                        int temp = array[j];
                        countAssign++;
                        array[j] = array[j + 1];
                        countAssign++;
                        array[j + 1] = temp;
                        countAssign++;
                    }
                }


            }
            countSum = countAssign + countCompara;
            bubbleSort[l][0] = k;
            bubbleSort[l][1] = countSum;
            l++;
        }

        return bubbleSort;


    }
}