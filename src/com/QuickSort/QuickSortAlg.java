package com.QuickSort;

import com.ArrayGenerator.ArraysGenerator;


public class QuickSortAlg {
    private static int countAssign;
    private static int countComp;
    private static int[] array;

    public static long[] quick(int complexity) {
        int l = 0;
        long[] quickSort = new long[100];

        for (int i = 100; i <= 10000; i += 100) {
            switch (complexity) {
                case 1:
                    array = ArraysGenerator.bestCaseArray(i);
                    break;
                case 2:
                    array = ArraysGenerator.averageCaseArray(i);
                    break;
                case 3:
                    array = ArraysGenerator.worstCaseArray(i);
                    break;
            }
            long countSum;
            countAssign = 0;
            countComp = 0;
            quickSort(array, 0, array.length - 1);
            countSum = countAssign + countComp;
            quickSort[l] = countSum;
            l++;
        }


        return quickSort;
    }

    private static void quickSort(int[] arr, int low, int high) {

        int middle = low + (high - low) / 2;
        countAssign++;
        int pivot = arr[middle];
        countAssign++;


        int i = low, j = high;
        countAssign += 2;
        countComp++;
        while (i <= j) {


            countComp++;
            while (arr[i] < pivot) {

                i++;
                countAssign++;
                countComp++;
            }

            countComp++;
            while (arr[j] > pivot) {

                j--;
                countAssign++;
                countComp++;
            }

            countComp++;
            if (i <= j) {

                swap(arr, i, j);
                i++;
                j--;
                countAssign += 2;
            }
        }

        countComp++;
        if (low < j) {

            quickSort(arr, low, j);
        }
        countComp++;
        if (high > i) {

            quickSort(arr, i, high);
        }
    }

    private static void swap(int array[], int x, int y) {
        int temp = array[x];
        countAssign++;
        array[x] = array[y];
        countAssign++;
        array[y] = temp;
        countAssign++;
    }
}
